import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, NavController,ViewController } from 'ionic-angular';

@Component({
  selector: 'page-subject-form',
  templateUrl: 'subject-form.html'
})

export class SubjectFormPage {
    private data : FormGroup;
    private oldData: any;
    nrc: any
    subject: any
    time: any
    build: any
    days: any
    teacher: any

    constructor(public navParams: NavParams, public viewCtrl: ViewController,
    public navCtrl: NavController,
                private formBuilder: FormBuilder) {
        this.teacher = this.navParams.get('prof');
        this.oldData = this.navParams.get('oldData');
        if(typeof this.oldData === 'undefined')
        {
            this.oldData = {nrc: '', name: '', section: '',
          time: '', place: ''};
        }

        this.data = this.formBuilder.group({
            nrc:
                [this.oldData.subject_code,
                 Validators.compose([Validators.pattern('[0-9]{6}'),
                                     Validators.required])],
            name:
                [this.oldData.name],
            time:
                [this.oldData.time,
                 Validators.compose([Validators.pattern('[0-2][0-9]:[0-5][0-9] *- *[0-2][0-9]:[0-5][0-9]'),
                                      Validators.required])],
            place:
                [this.oldData.place,
                  Validators.compose([Validators.pattern('(DED[A-Z]2*)|(DUCT(1|2))'),
                                      Validators.required])],
            days:
                [this.oldData.days,
                  Validators.compose([Validators.pattern('(L|M|I|J|V|S){1,6}'),
                                      Validators.required])]
        });
    }

    dismiss(data?: any) {
      this.viewCtrl.dismiss(data);
    }

    add() {
      let xhttp = new XMLHttpRequest();
      let _this = this;
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
        }
      };
      let qStr = encodeURI('https://rebeca290197.000webhostapp.com/subjects.php?method=POST&nrc=' + this.nrc + '&subject=' + this.subject + '&teacher=' + this.teacher + '&build=' + this.build + '&time=' + this.time + '&days=' + this.days)
      console.log(qStr)
      xhttp.open('GET', qStr, true);
      xhttp.send();
    }
}
