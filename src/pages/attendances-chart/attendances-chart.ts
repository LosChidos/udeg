import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AttendancesChartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-attendances-chart',
  templateUrl: 'attendances-chart.html',
})
export class AttendancesChartPage {
  nrc = ""
  chartOptions:any

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.nrc = navParams.get('nrc');
    this.getPercentages();
  }

  getPercentages() {
    let xhttp = new XMLHttpRequest();
    let _this = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        console.log(JSON.parse(xhttp.responseText))
        _this.setOptions(JSON.parse(xhttp.responseText))
      }
    };
    let qStr = 'https://rebeca290197.000webhostapp.com/attendances.php?method=GET&section=' + this.nrc;

    console.log(qStr)
    xhttp.open('GET', qStr, true);
    xhttp.send();
  }

  setOptions(vals) {
    this.chartOptions = {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: 'Porcetaje de asistencia de la sección '+ this.nrc + ' en el semestre'
      },
      tooltip: {
          valueSuffix: ''
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: false
              },
              showInLegend: true
          }
      },
      series: [{
          name: 'Porcentaje',
          colorByPoint: true,
          data: [{
              name: 'Faltas',
              color: 'red',
              y: parseInt(vals.abs)
          }, {
              name: 'Asistencias',
              color: 'blue',
              y: parseInt(vals.ats),
              sliced: true,
              selected: true
          }]
      }]
    }
  }

}
