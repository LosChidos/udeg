import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { SubjectFormPage } from '../subject-form/subject-form';
import { AttendancePage } from '../attendance/attendance';

@Component({
  selector: 'page-subjects-list',
  templateUrl: 'subjects-list.html',
})
export class SubjectsListPage {

  sections = [];
  days = "";
  prof = "";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController) {
    this.days = 'L';
    this.prof = this.navParams.get('prof');
  }

  ionViewDidLoad() {
    this.fetchSections();
  }

  addSubject(){
    console.log({'prof': this.prof})
    let modal = this.modalCtrl.create(SubjectFormPage, {'prof': this.prof});
    modal.present();
  }

  doRefresh(refresher) {
    setTimeout(() => {
        let xhttp = new XMLHttpRequest();
        let _t = this;
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log(JSON.parse(xhttp.responseText))
            _t.sections = []
            for (let section of JSON.parse(xhttp.responseText)) {
              section['color'] = "#" + intToRGB(hashCode(section.nrc + section.subject));
              _t.sections.push(section);
            }
            refresher.complete();
          }
        };
        let qStr = 'https://rebeca290197.000webhostapp.com/sections.php?method=GET&teacher=' + this.prof;
        console.log(qStr)
        xhttp.open('GET', qStr, true);
        xhttp.send();
    }, 2000);
  }

  takeAttendance(nrc) {
    // TODO send nrc to AttendancePage
    this.navCtrl.push(AttendancePage, {nrc: nrc});
  }

  fetchSections() {
    let xhttp = new XMLHttpRequest();
    let _this = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        console.log(JSON.parse(xhttp.responseText))
        _this.sections = []
        for (let section of JSON.parse(xhttp.responseText)) {
          section['color'] = "#" + intToRGB(hashCode(section.nrc + section.subject));
          _this.sections.push(section);
        }
      }
    };
    let qStr = 'https://rebeca290197.000webhostapp.com/sections.php?method=GET&teacher=' + this.prof;
    console.log(qStr)
    xhttp.open('GET', qStr, true);
    xhttp.send();
  }

  sectionsOf(day) {
    return this.sections.filter(section => section.days.search(day) != -1)
  }

}

function hashCode(str) { // java String#hashCode
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
     hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}

function intToRGB(i){
  var c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();

  return "00000".substring(0, 6 - c.length) + c;
}
