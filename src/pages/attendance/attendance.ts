import { Component } from '@angular/core';
import { NavParams,NavController, ModalController, ViewController,
         AlertController} from 'ionic-angular';

import { SubjectsListPage } from '../subjects-list/subjects-list';
import { AttendancesChartPage } from '../attendances-chart/attendances-chart';

@Component({
  selector: 'page-attendance',
  templateUrl: 'attendance.html'
})
export class AttendancePage {
    myDate: String = new Date().toISOString();
    nrc = ""
    students = []

    constructor(public navParams: NavParams,
                public viewCtrl: ViewController,
                public alertCtrl: AlertController,
                public navCtrl: NavController,
                public modalCtrl: ModalController) {
      this.nrc = navParams.get('nrc');
      this.fetchStudents()
    }

    fetchStudents() {
      let xhttp = new XMLHttpRequest();
      let _this = this;
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          _this.students = JSON.parse(xhttp.responseText);
        }
      };
      let qStr = 'https://rebeca290197.000webhostapp.com/get_students.php?section=' + this.nrc;
      xhttp.open('GET', qStr, true);
      xhttp.send();
    }

    setAttendance(id, type) {
      for (let student of this.students) {
        if (student.id == id) {
          student.type = type
          break
        }
      }
    }

    delete() {
      let xhttp = new XMLHttpRequest();
      let _this = this
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          _this.save()
        }
      };
      let qStr = 'https://rebeca290197.000webhostapp.com/set_attendances.php?delete=1&date=' + this.myDate.substr(0, 10) + '&section='+ this.nrc;
      xhttp.open('GET', qStr, true);
      xhttp.send()
    }

    save() {
      for (let student of this.students) {
        let xhttp = new XMLHttpRequest();
        let qStr = 'https://rebeca290197.000webhostapp.com/set_attendances.php?student='+ student.id + '&section='+ this.nrc + '&date='+ this.myDate.substr(0, 10) + '&attendance='+ student.type;
        xhttp.open('GET', qStr, true);
        xhttp.send()
      }
      this.navCtrl.pop();
    }

    getChart() {
      this.navCtrl.push(AttendancesChartPage, {nrc: this.nrc});
    }
}
