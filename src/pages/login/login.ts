import { Component } from '@angular/core';
import { NavController, ModalController, ToastController } from 'ionic-angular';
import { SubjectsListPage } from '../subjects-list/subjects-list';
import { NativeStorage } from '@ionic-native/native-storage';

import * as $ from 'jquery';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  id = "";
  nip = "";

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              private nativeStorage: NativeStorage,
              private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.nativeStorage.getItem('user').then(
      data => this.navCtrl.setRoot(SubjectsListPage),
      error => console.error(error)
    );
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  createProf() {
    let xhttp = new XMLHttpRequest();
    let _t = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        _t.navCtrl.setRoot(SubjectsListPage, {'prof': _t.id})
      }
    };
    let qStr = "https://rebeca290197.000webhostapp.com/teachers.php?method=POST&id=" + _t.id + "&name=" + _t.id;
    console.log(qStr)
    xhttp.open("GET", qStr, true)
    xhttp.send();
  }

  logIn() {
    let xhttp = new XMLHttpRequest;
    let _t = this;
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
        let res = xhttp.responseText.split(',');
        if(res[0] == "0" || res[0] == "T") {
          let toast = _t.toastCtrl.create({
            message: '¡Credenciales incorrectas!',
            duration: 2500
          });
          toast.present();
        } else {
          console.log({'prof': _t.id})
          _t.createProf()
            _t.navCtrl.setRoot(SubjectsListPage, {'prof': _t.id})
          // _t.menuCtrl.enable(true);
          // _t.nativeStorage.setItem('student', {code: _t.code}).then(
          //   () => _t.navCtrl.setRoot(TabsPage, {'student': _t.code}),
          //   error => {
          //     let toast = _t.toastCtrl.create({
          //       message: '¡Problema con NativeStorage!',
          //       duration: 2500
          //     });
          //     toast.present();
          //   }
          // );
        }
      }
    };
    xhttp.open("POST", "http://dcc.000webhostapp.com/pruebaLogin.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("codigo=" + this.id + "&nip=" + this.nip);
  }
}
