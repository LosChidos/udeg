import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { NativeStorage } from '@ionic-native/native-storage';
import { ChartModule } from 'angular2-highcharts';
import * as highcharts from 'highcharts';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { SubjectsListPage } from '../pages/subjects-list/subjects-list';
import { SubjectFormPage } from '../pages/subject-form/subject-form';
import { AttendancesChartPage } from '../pages/attendances-chart/attendances-chart';
import { AttendancePage } from '../pages/attendance/attendance';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SubjectsListPage,
    SubjectFormPage,
    AttendancesChartPage,
    AttendancePage
  ],
  imports: [
    BrowserModule,
    ChartModule.forRoot(highcharts),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SubjectsListPage,
    SubjectFormPage,
    AttendancesChartPage,
    AttendancePage
  ],
  providers: [
    StatusBar,
    NativeStorage,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
